# -*- coding: utf-8 -*-
import urllib.parse
from django.db import models
from django.urls import reverse
from cms.models import CMSPlugin
from pathlib import Path
import urllib.parse

from model_utils.models import TimeStampedModel


class TEIPublisher(TimeStampedModel):
    name = models.CharField(max_length=255, help_text="A unique name for this instance of TEI Publisher.")
    endpoint = models.CharField(max_length=1023, blank=True, default="", help_text="A URL to the endpoint of this TEI Publisher instance.")
    
    class Meta:
        verbose_name_plural = "TEI Publisher Instances"

    def __str__(self):
        return self.name or self.endpoint
    

class IIIFServer(TimeStampedModel):
    name = models.CharField(max_length=255, help_text="A unique name for this instance of this IIIF Server.")
    uri = models.CharField(max_length=1023, blank=False, help_text="A URL to the endpoint of this IIIF Server.")
    
    class Meta:
        verbose_name_plural = "IIIF Servers"
        verbose_name = "IIIF Server"

    def __str__(self):
        return self.name or self.uri
    
    def info_uri(self, identifier):
        identifier_string = str(identifier)
        # Check if we can escape this string
        if "%" not in identifier_string:
            identifier_string = urllib.parse.quote_plus(identifier_string)
        return Path(self.uri)/identifier_string/"info.json"


class TEIDoc(TimeStampedModel):
    name = models.CharField(max_length=255, help_text="A name/title for this TEI document.")
    publisher = models.ForeignKey(TEIPublisher, verbose_name="TEI Publisher Instance", on_delete=models.CASCADE, help_text="The instance of TEI Publisher hosting this document.")
    iiif = models.ForeignKey(IIIFServer, verbose_name="IIIF Server", default=None, blank=True, null=True, on_delete=models.SET_DEFAULT, help_text="A IIIF server hosting images referenced in this document.")
    path = models.CharField(max_length=1023, help_text="The path to the TEI XML file on the TEI Publisher instance.")
    odd = models.CharField(max_length=255, verbose_name="ODD", help_text="The ODD to use for this TEI document. Must be on the TEI Publisher instance.")
    template = models.CharField(max_length=255, blank=True, default="", help_text="The Django template to display this document. Template must be in Django instance. Default: tei/teidoc_detail.html")
    
    class Meta:
        verbose_name = "TEI Doc"
        verbose_name_plural = "TEI Docs"

    def __str__(self):
        return self.name or self.xml

    def get_absolute_url(self):
        return reverse('tei:TEIDoc_detail', kwargs=dict(pk=self.pk))

    def url_transformed(self, format):
        format = format.lower()
        if format not in ["pdf", "html", "epub"]:
            raise Exception("format must be one of pdf, html or epub")

        path_encoded = urllib.parse.quote_plus(self.path)

        # The API requires the extensino for the ODD
        odd = self.odd
        if not odd.endswith(".odd"):
            odd += ".odd"
        
        url = f"{self.publisher.endpoint}/api/document/{path_encoded}/{format}?odd={odd}&wc=true"
        return url
    
    def url_pdf(self):
        return self.url_transformed("pdf")

    def url_epub(self):
        return self.url_transformed("epub")

    def url_html(self):
        return self.url_transformed("html")


class TEIDocPluginModel(CMSPlugin):
    doc = models.ForeignKey(TEIDoc, on_delete=models.CASCADE)

    def __str__(self):
        return self.doc.name


class IIIFImagePluginModel(CMSPlugin):
    identifier = models.CharField(max_length=255, help_text="The identifier of this image on the IIIF server.")
    iiif = models.ForeignKey(IIIFServer, verbose_name="IIIF Server", default=None, blank=True, null=True, on_delete=models.SET_DEFAULT, help_text="The IIIF server hosting this image.")

    def __str__(self):
        return self.identifier

    def info_uri(self):
        return self.iiif.info_uri( self.identifier )


class IIIFPresentationPluginModel(CMSPlugin):
    manifest_uri = models.CharField(max_length=1023, help_text="The URI to the manifest for this IIIF presentation.")
    width = models.CharField(max_length=31, default="100%", help_text="The width of the presentation when displayed.")
    height = models.CharField(max_length=31, default="500px", help_text="The height of the presentation when displayed.")

    def __str__(self):
        return self.manifest_uri