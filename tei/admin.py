# -*- coding: utf-8 -*-

from django.contrib import admin

from .models import (
	TEIDoc,
	TEIPublisher,
    IIIFServer,
)


@admin.register(TEIDoc)
class TEIDocAdmin(admin.ModelAdmin):
    pass


@admin.register(IIIFServer)
class IIIFServerAdmin(admin.ModelAdmin):
    pass


@admin.register(TEIPublisher)
class TEIPublisherAdmin(admin.ModelAdmin):
    pass



