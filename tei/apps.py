# -*- coding: utf-8
from django.apps import AppConfig


class TeiConfig(AppConfig):
    name = 'tei'
