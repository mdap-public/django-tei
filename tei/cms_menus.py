from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from cms.menu_bases import CMSAttachMenu
from menus.base import NavigationNode
from menus.menu_pool import menu_pool

from .models import TEIDoc

class TEIMenu(CMSAttachMenu):
    name = _("TEI Menu")  # give the menu a name this is required.

    def get_nodes(self, request):
        """
        This method is used to build the menu tree.
        """
        nodes = []
        for doc in TEIDoc.objects.all():
            node = NavigationNode(
                title=doc.name,
                url=doc.get_absolute_url(),
                id=doc.pk,  # unique id for this node within the menu
            )
            nodes.append(node)
        return nodes

menu_pool.register_menu(TEIMenu)
