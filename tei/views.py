# -*- coding: utf-8 -*-
from django.views.generic import (
    CreateView,
    DeleteView,
    DetailView,
    UpdateView,
    ListView
)

from .models import (
	TEIDoc,
	TEIPublisher,
    IIIFPresentationPluginModel,
)


class TEIDocCreateView(CreateView):
    model = TEIDoc


class TEIDocDeleteView(DeleteView):
    model = TEIDoc


class TEIDocDetailView(DetailView):
    model = TEIDoc

    def get_template_names(self):
        """ First tries the template name from the object. """
        self.object = self.get_object()
        if self.object.template:
            return [self.object.template]
        return super().get_template_names()


class TEIDocUpdateView(UpdateView):

    model = TEIDoc


class TEIDocListView(ListView):
    model = TEIDoc
    paginate_by = "50"


class TEIPublisherCreateView(CreateView):
    model = TEIPublisher


class TEIPublisherDeleteView(DeleteView):
    model = TEIPublisher


class TEIPublisherDetailView(DetailView):
    model = TEIPublisher


class TEIPublisherUpdateView(UpdateView):
    model = TEIPublisher


class TEIPublisherListView(ListView):
    model = TEIPublisher

class IIIFPresentationPluginModelDetailView(DetailView):
    model = IIIFPresentationPluginModel
    template_name = "tei/iiif-presentation-detail.html"
    context_object_name = "instance"