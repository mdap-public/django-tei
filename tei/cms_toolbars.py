from cms.toolbar_base import CMSToolbar
from cms.toolbar_pool import toolbar_pool
from cms.utils.urlutils import admin_reverse

from .models import TEIDoc


class TEIToolbar(CMSToolbar):

    def populate(self):
        # if not self.is_current_app:
        #     return

        menu = self.toolbar.get_or_create_menu(
            'tei',  # a unique key for this menu
            'TEI',  # the text that should appear in the menu
        )
        menu.add_sideframe_item(
            name='TEI Docs',                          # name of the new menu item
            url=admin_reverse('tei_teidoc_changelist'),    # the URL it should open with
        )
        menu.add_modal_item(
            name='Add new TEI Doc',        # name of the new menu item
            url=admin_reverse('tei_teidoc_add'),  # the URL it should open with
        )
        # menu.add_modal_item(
        #     name='Add new IIIF Server',
        #     url=admin_reverse('tei_iiifserver_add'),
        # )



toolbar_pool.register(TEIToolbar)
