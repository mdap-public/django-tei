# Generated by Django 3.1.3 on 2021-04-20 08:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tei', '0004_auto_20210420_0803'),
    ]

    operations = [
        migrations.AddField(
            model_name='teidoc',
            name='template',
            field=models.CharField(blank=True, default='', help_text='The Django template to display this document. Template must be in Django instance. Default: tei/teidoc_detail.html', max_length=255),
        ),
        migrations.AlterField(
            model_name='teidoc',
            name='name',
            field=models.CharField(help_text='A name/title for this TEI document.', max_length=255),
        ),
    ]
