from django.utils.translation import gettext as _
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from . import models

@plugin_pool.register_plugin  # register the plugin
class TEIDocPluginPublisher(CMSPluginBase):
    model = models.TEIDocPluginModel
    module = _("TEI")
    name = _("TEI Document")  # name of the plugin in the interface
    render_template = "tei/teidoc_plugin.html"

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context


@plugin_pool.register_plugin
class IIIFImagePluginPublisher(CMSPluginBase):
    model = models.IIIFImagePluginModel
    module = _("TEI")
    name = _("IIIF Image")
    render_template = "tei/iiif-image.html"


@plugin_pool.register_plugin
class IIIFPresentationPluginPublisher(CMSPluginBase):
    model = models.IIIFPresentationPluginModel
    module = _("TEI")
    name = _("IIIF Presentation")
    render_template = "tei/iiif-presentation.html"


    