# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.urls import path
from django.views.generic import TemplateView

from . import views


app_name = 'tei'
urlpatterns = [
    path('iiif/presentation/<int:pk>/', views.IIIFPresentationPluginModelDetailView.as_view(), name='iiif-presentation-detail'),
]
