============
Installation
============

At the command line::

    $ easy_install django-tei

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv django-tei
    $ pip install django-tei
