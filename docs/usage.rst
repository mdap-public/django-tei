=====
Usage
=====

To use Django TEI in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'tei.apps.TeiConfig',
        ...
    )

Add Django TEI's URL patterns:

.. code-block:: python

    from tei import urls as tei_urls


    urlpatterns = [
        ...
        url(r'^', include(tei_urls)),
        ...
    ]
