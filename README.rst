=============================
Django TEI
=============================

.. image:: https://badge.fury.io/py/django-tei.svg
    :target: https://badge.fury.io/py/django-tei

.. image:: https://travis-ci.org/rbturnbull/django-tei.svg?branch=master
    :target: https://travis-ci.org/rbturnbull/django-tei

.. image:: https://codecov.io/gh/rbturnbull/django-tei/branch/master/graph/badge.svg
    :target: https://codecov.io/gh/rbturnbull/django-tei

A Django app to display TEI editions using TEI publisher.

Documentation
-------------

The full documentation is at https://django-tei.readthedocs.io.

Quickstart
----------

Install Django TEI::

    pip install django-tei

Add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'tei.apps.TeiConfig',
        ...
    )

Add Django TEI's URL patterns:

.. code-block:: python

    from tei import urls as tei_urls


    urlpatterns = [
        ...
        url(r'^', include(tei_urls)),
        ...
    ]

Features
--------

* TODO

Running Tests
-------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install tox
    (myenv) $ tox


Development commands
---------------------

::

    pip install -r requirements_dev.txt
    invoke -l


Credits
-------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-djangopackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-djangopackage`: https://github.com/pydanny/cookiecutter-djangopackage
