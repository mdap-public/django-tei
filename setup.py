#!/usr/bin/env python

# Taken from https://snarky.ca/what-the-heck-is-pyproject-toml/
import setuptools

if __name__ == "__main__":
    setuptools.setup()
